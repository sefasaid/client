# Task

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Testing App

Just go [Firebase App](https://anket-bccf7.web.app/)

## To run socket.io server

Clone this [bitbucket](https://bitbucket.org/sefasaid/api) repo. And start with `npm run start `

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `public` directory. Use the `-c production` flag for a production build.

## Login to app

To login with owner owner:owner

To login with user user:user

## Project Structure

- [App Module](./src/app/app.module.ts)
- [Auth Module](./src/app/auth/auth-module.ts)
  - [Login Component](./src/app/auth/login/login.component.ts)
- [Restricted Module](./src/app/restricted/restricted.module.ts)

  - [Home Component](./src/app/restricted/home/home.component.ts)
    - [Add Component](./src/app/restricted/home/add/add.component.ts)
    - [Bars Component](./src/app/restricted/home/bars/bars.component.ts)
    - [Add Component](./src/app/restricted/home/vote/vote.component.ts)

- [README.md](./README.md)

## Project explanation

This project is based 3 modules. First `App Module` is initial module

`Auth Module` contains login page.

`Restricted Module` contains home page. And `Home Page` contains 3 components. And this module guarded by `auth.guard`

For chart I used [ngx-charts](https://swimlane.gitbook.io/ngx-charts/)

For layout [flex-layout](https://github.com/angular/flex-layout)

Of course [Angular Material](https://material.angular.io/)
