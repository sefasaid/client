import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { PollService } from '../services/poll.service';

@Component({
  selector: 'app-restricted',
  templateUrl: './restricted.component.html',
  styleUrls: ['./restricted.component.scss']
})
export class RestrictedComponent implements OnInit {

  constructor(
    private pollService: PollService,
    private authService: AuthService,
  ) {

  }

  ngOnInit(): void {
    this.pollService.getPoll();
  }

  logout() {
    this.authService.logout();
  }

}
