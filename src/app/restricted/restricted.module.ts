import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestrictedRoutingModule } from './restricted-routing.module';
import { HomeComponent } from './home/home.component';
import { RestrictedComponent } from './restricted.component';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AddComponent } from './home/add/add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VoteComponent } from './home/vote/vote.component';
import { BarsComponent } from './home/bars/bars.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@NgModule({
  declarations: [
    HomeComponent,
    RestrictedComponent,
    AddComponent,
    VoteComponent,
    BarsComponent
  ],
  imports: [
    CommonModule,
    RestrictedRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
  ]
})
export class RestrictedModule { }
