import { Component, OnDestroy } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ChartDataModel } from 'src/app/models/chart.model';
import { PollService } from 'src/app/services/poll.service';

@Component({
  selector: 'app-bars',
  templateUrl: './bars.component.html',
  styleUrls: ['./bars.component.scss']
})
export class BarsComponent implements OnDestroy {
  public data: ChartDataModel[] = [];
  public colorScheme = {
    domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#a95963', '#8796c0']
  };
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private pollService: PollService,
  ) {
    this.pollService.activePoll$.pipe(takeUntil(this.destroyed$)).subscribe((res) => {
      if (res) {
        this.data = res.options.map((option) => { return { name: option.name, value: option.votes ?? 0 } as ChartDataModel });
      } else {
        this.data = [];
      }
    });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  onSelect(event: any) {
    console.log(event);
  }

}
