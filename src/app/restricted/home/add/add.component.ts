import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OptionModel } from 'src/app/models/poll.model';
import { PollService } from 'src/app/services/poll.service';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit, OnDestroy {

  public pollForm!: FormGroup;
  public get options(): FormArray {
    return this.pollForm.get('options') as FormArray
  }
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  constructor(
    private formBuilder: FormBuilder,
    private pollService: PollService,
  ) {

  }


  ngOnInit(): void {
    this.pollForm = this.formBuilder.group({
      name: ["", [Validators.required, Validators.min(4), Validators.maxLength(80)]],
      options: this.formBuilder.array([this.newOption(),this.newOption()]),
    });
    this.pollService.activePoll$.pipe(takeUntil(this.destroyed$)).subscribe((res) => {
      if (res) {
        this.pollForm.patchValue({
          name: res.name
        });
        let formArray = this.pollForm.controls['options'] as FormArray;
        while (formArray.length !== 0) {
          formArray.removeAt(0)
        }
        res.options.forEach((option: OptionModel) => {
          formArray.push(this.newOption(option.name, option.votes));
        })
      }
    })
  }
  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
  newOption(initValue?: string, initVote?: number): FormGroup {
    return this.formBuilder.group({
      name: [initValue ?? '', [Validators.required, Validators.min(4), Validators.maxLength(80)]],
      votes: [initVote ?? 0]
    });
  }

  public addOption() {
    this.options.push(this.newOption());
  }
  public onSubmit() {
    this.pollService.savePoll(this.pollForm.value);
  }

  public removeOption(i: number) {
    this.options.removeAt(i);
  }
  public resetAll() {
    this.pollForm.reset();
    let formArray = this.pollForm.controls['options'] as FormArray;
    while (formArray.length !== 2) {
      formArray.removeAt(0)
    }
    this.pollService.resetPoll();
  }

}
