import { PollService } from 'src/app/services/poll.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { PollModel } from 'src/app/models/poll.model';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss']
})
export class VoteComponent implements OnDestroy {

  public poll: PollModel | undefined;
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private pollService: PollService,
  ) {
    this.pollService.activePoll$.pipe(takeUntil(this.destroyed$)).subscribe((res) => {
      this.poll = res;
    });
  }
  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  vote(index: number) {
    if (this.poll) {
      this.poll.options[index].votes++;
      this.pollService.savePoll(this.poll);
    }
  }
}
