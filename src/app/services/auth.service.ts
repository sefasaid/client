import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { UserModel } from '../models/user.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private users: UserModel[] = [
    { userName: 'owner', password: 'owner', role: 'Owner' },
    { userName: 'user', password: 'user', role: 'User' }
  ];
  public loggedUser$: Subject<UserModel | undefined> = new BehaviorSubject<UserModel | undefined>(undefined);

  constructor(
    private router: Router,
    private snackbar: MatSnackBar,
  ) {
    const user = localStorage.getItem('user');
    if (!!user) {
      this.loggedUser$.next(JSON.parse(user));
    }
  }

  login(userName: string, password: string) {
    const user = this.users.find(user => user.userName === userName && user.password === password);
    if (!!user) {
      this.loggedUser$.next(user);
      localStorage.setItem('user', JSON.stringify(user));
      this.router.navigate(['/app/home']);
    } else {
      this.snackbar.open('User not found', 'Ok', {
        duration: 3000
      });
    }
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/']);

  }
}
