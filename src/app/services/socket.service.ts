import { Injectable } from '@angular/core';
import { PollModel } from '../models/poll.model';
import { Socket } from 'ngx-socket-io';
@Injectable({
  providedIn: 'root'
})
export class SocketService {
  constructor(
    private socket: Socket
  ) {
  }

  sendPollUpdate(poll: PollModel | undefined) {
    this.socket.emit('poll', poll);
  }

  getPollEvent() {
    return this.socket.fromEvent('poll');
  }
}
