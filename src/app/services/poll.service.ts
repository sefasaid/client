import { PollModel } from './../models/poll.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { SocketService } from './socket.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PollService {

  public activePoll$: Subject<PollModel | undefined> = new BehaviorSubject<PollModel | undefined>(undefined);

  constructor(
    private socketService: SocketService
  ) {
    this.socketService.getPollEvent().subscribe((res: any) => {
      this.savePoll(res, false);
    })
  }
  getPoll() {
    const poll = localStorage.getItem('poll');
    if (!!poll) {
      this.activePoll$.next(JSON.parse(poll) as PollModel);
    }
  }

  savePoll(poll: PollModel, updateOthers: boolean = true) {
    this.activePoll$.next(poll);
    localStorage.setItem('poll', JSON.stringify(poll));
    if (updateOthers) this.socketService.sendPollUpdate(poll);
  }
  resetPoll() {
    this.activePoll$.next(undefined);
    localStorage.removeItem('poll');
    this.socketService.sendPollUpdate(undefined);
  }
}
