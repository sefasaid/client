export class PollModel {
    name!: string;
    options!: OptionModel[];
}
export class OptionModel {
    name!: string;
    votes!: number;
}
