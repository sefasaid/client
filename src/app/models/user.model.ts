export class UserModel {
    userName!: string;
    password!: string;
    role!: 'Owner' | 'User';
}
